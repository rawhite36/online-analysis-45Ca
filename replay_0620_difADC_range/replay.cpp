//replay analysis class


#ifndef replay_C
#define replay_C

#include "inc/replay.hpp"
#include "inc/replay_gui.cpp"


replay::replay(Int_t fileNum)
{
  fNum=fileNum; fCnt=-1;
  initFlag=0;
  statInitFlag=0;
  strap_length=2*SRT+SFT+1;
  ltrap_length=2*LRT+LFT+1;
  entry=-1; entry2=-1; entry3=-1;
  error=0; clbFlag=0;
  data=NULL; strap=NULL; ltrap=NULL;
  fin=NULL; fin2=NULL; fin3=NULL;
  hist=NULL;
  grWF=NULL;
  ftime=NULL; rtime=NULL; itime=NULL;
  slowData=NULL;
  gui=NULL;

  nentriesT = new Long64_t[fNum];
  gCnt = new Long64_t[fNum];

  engy_spec = new Double_t*[fNum];
  classType = new Byte_t*[fNum];
  bdchf = new Int_t*[fNum];
  bdch = new Int_t**[fNum];
  timWF = new ULong64_t*[fNum];

  for(Int_t i=0; i<fNum; ++i)
  {
    gCnt[i]=0;

    engy_spec[i]=NULL;
    classType[i]=NULL;
    bdchf[i]=NULL;
    bdch[i]=NULL;
    timWF[i]=NULL;
  }

  histE = new TH1D*[5];

  for(Short_t i=0; i<5; ++i)
    histE[i] = NULL;

  gr2 = new TGraph*[7];

  for(Short_t i=0; i<7; ++i)
    gr2[i] = NULL;

  for(Short_t i=0; i<6; ++i)
    for(Short_t j=0; j<8; ++j)
    {
      calib[i][j][0]=0;
      calib[i][j][1]=1;
      calib[i][j][2]=0;
      dec[i][j]=DEC;
      decCnt[i][j]=0;
    }
}

//-------------------------------------------------------------------------------------------------

replay::~replay()
{
  closef();

  delete[] gCnt;

  if(data!=NULL)
    delete[] data;

  if(strap!=NULL)
    delete[] strap;

  if(ltrap!=NULL)
    delete[] ltrap;

  for(UInt_t i=0; i<gr.size(); ++i)
    delete gr[i];

  for(UInt_t i=0; i<can.size(); ++i)
    if(gROOT->GetListOfCanvases()->FindObject(can[i]))
      delete can[i];

  if(grWF!=NULL)
    delete grWF;

  for(Short_t i=0; i<5; ++i)
    if(histE[i]!=NULL)
      delete histE[i];

  delete[] histE;

  for(Short_t i=0; i<7; ++i)
    if(gr2[i]!=NULL)
      delete gr2[i];

  delete[] gr2;

  for(Int_t i=0; i<fNum; ++i)
    if(classType[i]!=NULL)
      delete[] classType[i];

  for(Int_t i=0; i<fNum; ++i)
    if(engy_spec[i]!=NULL)
      delete[] engy_spec[i];

  for(Int_t i=0; i<fNum; ++i)
    if(bdchf[i]!=NULL)
      delete[] bdchf[i];

  for(Int_t i=0; i<fNum; ++i)
    if(bdch[i]!=NULL)
    {
      delete[] bdch[i][0];
      delete[] bdch[i][1];
      delete[] bdch[i];
    }

  for(Int_t i=0; i<fNum; ++i)
    if(timWF[i]!=NULL)
      delete[] timWF[i];

  delete[] engy_spec;
  delete[] classType;
  delete[] bdchf;
  delete[] bdch;
  delete[] timWF;

  if(ftime!=NULL)
    delete[] ftime;

  if(rtime!=NULL)
    delete[] rtime;

  if(itime!=NULL)
    delete[] itime;

  if(slowData!=NULL)
  {
    for(Short_t i=0; i<7; ++i)
      delete[] slowData[i];

    delete[] slowData;
  }

  if(error==2 || error==3)
    delete gui;
}

//-------------------------------------------------------------------------------------------------

Byte_t replay::openf(string fname)
{
  ++fCnt;

  if(fin!=NULL)
    delete fin;

  fin = new ifstream(fname,ios::binary);

  if(fin->is_open())
  {
    fin->seekg(37,fin->beg);
    fin->read((Char_t*)&length,4);

    fin->seekg(0,fin->end);

    nentries=fin->tellg();
    nentries=(nentries-1000*(length*2+33)-8)/(length*2+33);

    nentriesT[fCnt]=nentries;

    fin->seekg(1000*(length*2+33)+8,fin->beg);

    engy_spec[fCnt] = new Double_t[nentries];
    classType[fCnt] = new Byte_t[nentries];
    bdchf[fCnt] = new Int_t[nentries];
    bdch[fCnt] = new Int_t*[2];
    bdch[fCnt][0] = new Int_t[nentries];
    bdch[fCnt][1] = new Int_t[nentries];
    timWF[fCnt] = new ULong64_t[nentries];

    if(fCnt==fNum-1)
    {
      gui = new tgui;
      gui->input_data(nentries,this);
      gui->maketabs();
    }
  }
  else
  {
    cout << "\nCannot Open WF File\n\n";
    fin = NULL;
    error=1;
  }

  entry=-1;

  if(fin!=NULL)
    return 0;
  else
    return 1;
}

//-------------------------------------------------------------------------------------------------

Byte_t replay::openf2(string fname)
{
  fin2 = new ifstream(fname,ios::binary);

  if(fin2->is_open())
  {
    fin2->seekg(0,fin2->end);

    nentries2=fin2->tellg();
    nentries2=(nentries2-41000)/41;

    fin2->seekg(41000,fin2->beg);

    ftime = new Float_t[nentries2];
    rtime = new Float_t[nentries2];
    itime = new Float_t[nentries2];

    for(Short_t i=0; i<6; ++i)
      for(Short_t j=0; j<8; ++j)
      {
        dec[i][j]=0.;
        decCnt[i][j]=0;
      }
  }
  else
  {
    cout << "\nCannot Open Fitting File\n\n";
    fin2 = NULL;
    error=2;
  }

  entry2=-1;

  if(fin2!=NULL)
    return 0;
  else
    return 1;
}

//-------------------------------------------------------------------------------------------------

Byte_t replay::openf3(string fname)
{
  fin3 = new ifstream(fname,ios::binary);

  if(fin3->is_open())
  {
    fin3->seekg(0,fin3->end);

    nentries3=fin3->tellg();
    nentries3=(nentries3-8)/56;

    fin3->seekg(8,fin3->beg);

    slowData = new Double_t*[7];
    for(Short_t i=0; i<7; ++i)
      slowData[i] = new Double_t[nentries3];
  }
  else
  {
    cout << "\nCannot Open Slow Control File\n\n";
    fin3 = NULL;
    error=3;
  }

  entry3=-1;

  if(fin3!=NULL)
    return 0;
  else
    return 1;
}

//-------------------------------------------------------------------------------------------------

void replay::closef()
{
  if(fin!=NULL)
    delete fin;

  if(fin2!=NULL)
    delete fin2;

  if(fin3!=NULL)
    delete fin3;

  fin = NULL;
  fin2 = NULL;
  fin3 = NULL;
}

//-------------------------------------------------------------------------------------------------

void replay::read()
{
  if(entry!=nentries)
  {
    fin->read((Char_t*)&result,1);
    fin->seekg(4,fin->cur);
    fin->read((Char_t*)&bd,4);
    fin->read((Char_t*)&ch,4);
    fin->read((Char_t*)&timR,8);
    fin->read((Char_t*)&timC,8);
    fin->read((Char_t*)&length,4);

    if(initFlag==0)
    {
      data = new Short_t[length];
      strap = new Double_t[length];
      ltrap = new Double_t[length];
      initFlag=1;
    }

    fin->read((Char_t*)data,length*2);

    if(ch==0)
    {
      engy_spec[fCnt][gCnt[fCnt]]=0.;
      classType[fCnt][gCnt[fCnt]]=40;
      bdchf[fCnt][gCnt[fCnt]]=8*bd+ch;
      ++gCnt[fCnt];
    }

    if(result==2)
    {
      engy_spec[fCnt][gCnt[fCnt]]=0.;
      classType[fCnt][gCnt[fCnt]]=41;
      bdchf[fCnt][gCnt[fCnt]]=8*bd+ch;
      ++gCnt[fCnt];
    }

    if(result==0)
      error=5;

    for(UShort_t i=0; i<length; ++i)
    {
      data[i] = data[i] & 16383;

      if(data[i]>8191)
        data[i]-=16384;
    }

    ++entry;

    bdch[fCnt][0][entry]=bd;
    bdch[fCnt][1][entry]=ch;
    timWF[fCnt][entry]=timR;
  }
  else
    cout << "\nCannot Read Entry, End Of WF File Has Been Reached\n\n";
}

//-------------------------------------------------------------------------------------------------

void replay::read(Long64_t num, Byte_t flag)
{
  if(flag==0)
  {
    if(num>-1)
    {
      if(num<nentries)
      {
        if(ifstream::eofbit)
          fin->clear();

        fin->seekg(1000*(length*2+33)+8+num*(length*2+33),fin->beg);

        fin->read((Char_t*)&result,1);
        fin->seekg(4,fin->cur);
        fin->read((Char_t*)&bd,4);
        fin->read((Char_t*)&ch,4);
        fin->read((Char_t*)&timR,8);
        fin->read((Char_t*)&timC,8);
        fin->read((Char_t*)&length,4);

        if(initFlag==0)
        {
          data = new Short_t[length];
          strap = new Double_t[length];
          ltrap = new Double_t[length];
          initFlag=1;
        }

        fin->read((Char_t*)data,length*2);

        for(UShort_t i=0; i<length; ++i)
        {
          data[i] = data[i] & 16383;

          if(data[i]>8191)
            data[i]-=16384;
        }

        entry=num;
      }
      else
        cout << "\nCannot Read Entry, End Of WF File Has Been Reached\n\n";
    }
    else
    {
      entry=-1;
      fin->seekg(1000*(length*2+33)+8,fin->beg);
    }
  }
  else
  {
    if(entry+num<nentries)
      {
        if(ifstream::eofbit)
          fin->clear();

        fin->seekg((num-1)*(length*2+33),fin->cur);

        fin->read((Char_t*)&result,1);
        fin->seekg(4,fin->cur);
        fin->read((Char_t*)&bd,4);
        fin->read((Char_t*)&ch,4);
        fin->read((Char_t*)&timR,8);
        fin->read((Char_t*)&timC,8);
        fin->read((Char_t*)&length,4);

        if(initFlag==0)
        {
          data = new Short_t[length];
          strap = new Double_t[length];
          ltrap = new Double_t[length];
          initFlag=1;
        }

        fin->read((Char_t*)data,length*2);

        for(UShort_t i=0; i<length; ++i)
        {
          data[i] = data[i] & 16383;

          if(data[i]>8191)
            data[i]-=16384;
        }

        entry+=num;
      }
      else
        cout << "\nCannot Read Entry, End Of WF File Has Been Reached\n\n";
  }
}

//-------------------------------------------------------------------------------------------------

void replay::read2()
{
  if(entry2!=nentries2)
  {
    fin2->read((Char_t*)&result2,1);
    fin2->seekg(4,fin->cur);
    fin2->read((Char_t*)&bd2,4);
    fin2->read((Char_t*)&ch2,4);
    fin2->read((Char_t*)&tim2,8);
    fin2->read((Char_t*)&fallTime,4);
    fin2->read((Char_t*)&riseTime,4);
    fin2->read((Char_t*)&v0,4);
    fin2->read((Char_t*)&t0,4);
    fin2->read((Char_t*)&chi,4);

    ++entry2;

    ftime[entry2]=fallTime;
    rtime[entry2]=riseTime;
    itime[entry2]=t0;

    if(bd2>=0 && bd2<6 && ch2>=0 && ch2<8 && chi>0.95 && chi<1.05)
    {
      dec[bd2][ch2]+=fallTime;
      ++decCnt[bd2][ch2];
    }
  }
  else
    cout << "\nCannot Read Entry, End Of Fitting File Has Been Reached\n\n";
}

//-------------------------------------------------------------------------------------------------

void replay::read3()
{
  if(entry3!=nentries3)
  {
    ++entry3;

    fin3->read((Char_t*)&slowData[0][entry3],8);
    fin3->read((Char_t*)&slowData[1][entry3],8);
    fin3->read((Char_t*)&slowData[2][entry3],8);
    fin3->read((Char_t*)&slowData[3][entry3],8);
    fin3->read((Char_t*)&slowData[4][entry3],8);
    fin3->read((Char_t*)&slowData[5][entry3],8);
    fin3->read((Char_t*)&slowData[6][entry3],8);

    slowData[5][entry3]*=1000000000;
    slowData[6][entry3]*=1000000000;
  }
  else
    cout << "\nCannot Read Entry, End Of Slow Control File Has Been Reached\n\n";
}

//-------------------------------------------------------------------------------------------------

Byte_t replay::calibrate()
{
  string stemp;
  Char_t ctemp = ' ';

  ifstream fin4("calib/calib.txt");

  if(fin4.is_open())
  {
    for(Short_t i=0; i<400 && ctemp!='*'; ++i)
      fin4.read(&ctemp,1);

    if(ctemp!='*')
    {
      error=4;
      return 1;
    }

    for(Short_t i=0; i<6; ++i)
    {
      for(Short_t i=0; i<200 && ctemp!=':'; ++i)
        fin4.read(&ctemp,1);

      if(ctemp!=':')
      {
        error=4;
        return 1;
      }

      fin4.read(&ctemp,1);

      for(Short_t j=0; j<8; ++j)
      {
        for(Short_t i=0; i<200 && ctemp!=':'; ++i)
          fin4.read(&ctemp,1);

        if(ctemp!=':')
        {
          error=4;
          return 1;
        }

        fin4.read(&ctemp,1);

        while(ctemp!=' ' && ctemp!='\n')
          fin4.read(&ctemp,1);

        fin4.read(&ctemp,1);

        while(ctemp!=' ' && ctemp!='\n')
        {
          stemp+=ctemp;
          fin4.read(&ctemp,1);
        }

        if(stemp.size()>0)
          calib[i][j][0] = stod(stemp);
        else
        {
          error=4;
          return 1;
        }

        stemp.clear();
        fin4.read(&ctemp,1);

        while(ctemp!=' ' && ctemp!='\n')
        {
          stemp+=ctemp;
          fin4.read(&ctemp,1);
        }

        if(stemp.size()>0)
          calib[i][j][1] = stod(stemp);
        else
        {
          error=4;
          return 1;
        }

        stemp.clear();
        fin4.read(&ctemp,1);

        while(ctemp!=' ' && ctemp!='\n')
        {
          stemp+=ctemp;
          fin4.read(&ctemp,1);
        }

        if(stemp.size()>0)
          calib[i][j][2] = stod(stemp);
        else
        {
          error=4;
          return 1;
        }

        stemp.clear();
      }
    }
  }
  else
  {
    error=4;
    return 1;
  }

  clbFlag=1;
  return 0;
}

//-------------------------------------------------------------------------------------------------

void replay::getDecConst()
{
  for(Short_t i=0; i<6; ++i)
    for(Short_t j=0; j<8; ++j)
      if(decCnt[i][j]!=0)
        dec[i][j]/=decCnt[i][j];
      else
        dec[i][j]=DEC;
}

//-------------------------------------------------------------------------------------------------

void replay::exp_fit()
{
  Short_t maxx;
  Short_t maxy;
  Int_t *x;
  Int_t *y;
  TGraph* fitgr;
  TF1 *fit;

  for(Short_t i=0; i<6; ++i)
    for(Short_t j=0; j<8; ++j)
    {
      dec[i][j]=0;
      decCnt[i][j]=0;
    }

  for(Int_t i=0; (nentries>50000) ? (i<50000) : (i<nentries); ++i)
  {
    read(i,0);

    sub_base();

    maxx=0;
    maxy=data[0];

    for(Short_t j=1; j<2500; ++j)
      if(data[j]>maxy)
      {
        maxx=j;
        maxy=data[j];
      }

    for(Short_t j=0; j<5; ++j)
      {
        maxy+=data[maxx-j-1];
        maxy+=data[maxx+j+1];
      }

    maxy/=11;

    if(maxx<1100 && maxx>900)
    {
      maxx+=20;
      x = new Int_t[2500-maxx];
      y = new Int_t[2500-maxx];

      for(Short_t j=0; j<2500-maxx; ++j)
      {
        x[j]=j;
        y[j]=data[maxx+j];
      }

      fitgr = new TGraph(2500-maxx, x, y);
	    fit = new TF1("fit", "[0]*exp(-x*[1])");	
      fitgr->Fit("fit", "FQ");

      if(1/fit->GetParameter(1)>900. && 1/fit->GetParameter(1)<1100.)
      {
        dec[bd][ch]+=1/fit->GetParameter(1);
        ++decCnt[bd][ch];
      }

      delete[] x;
      delete[] y;
      delete fitgr;
      delete fit;
    }
  }

  for(Short_t i=0; i<6; ++i)
    for(Short_t j=0; j<8; ++j)
      if(decCnt[i][j]!=0)
        dec[i][j]/=decCnt[i][j];
      else
        dec[i][j]=DEC;

  read(-1,0);
}

//-------------------------------------------------------------------------------------------------

void replay::sub_base()
{
  bmean=0;

  for(Short_t i=0; i<850; ++i)
    bmean+=data[i];

  bmean/=850;

  for(Short_t i=0; i<length; ++i)
    data[i]-=bmean;
}

//-------------------------------------------------------------------------------------------------

void replay::short_trap()
{
  s=0.; p=0; d=0;

  for(Short_t i=0; i<strap_length; ++i)
    sfilt[i]=0;

  for(Short_t i=0; i<length; ++i)
  {
    sfilt[i%strap_length]=data[i];

    d=sfilt[i%strap_length]-sfilt[(i+strap_length-SRT)%strap_length]
     -sfilt[(i+strap_length-SRT-SFT)%strap_length]+sfilt[(i+1)%strap_length];
    p+=d;
    s+=p+dec[bd][ch]*(Double_t)d;

    strap[i]=s/((Double_t)SRT*dec[bd][ch]);
  }
}

//-------------------------------------------------------------------------------------------------

void replay::long_trap()
{
  s=0.; p=0; d=0;

  for(Short_t i=0; i<ltrap_length; ++i)
    lfilt[i]=0;

  for(Short_t i=0; i<length; ++i)
  {
    lfilt[i%ltrap_length]=data[i];

    d=lfilt[i%ltrap_length]-lfilt[(i+ltrap_length-LRT)%ltrap_length]
     -lfilt[(i+ltrap_length-LRT-LFT)%ltrap_length]+lfilt[(i+1)%ltrap_length];
    p+=d;
    s+=p+dec[bd][ch]*(Double_t)d;

    ltrap[i]=s/((Double_t)LRT*dec[bd][ch]);
  }
}

//-------------------------------------------------------------------------------------------------

void replay::view(Byte_t flag)
{
  can.push_back(NULL);
  can.back() = new TCanvas;

  if(flag==0)
  {
    Int_t x[length];
    Int_t y[length];

    for(Short_t i=0; i<length; ++i)
    {
      x[i]=i;
      y[i]=data[i];
    }

    gr.push_back(NULL);
    gr.back() = new TGraph(length,x,y);
    gr.back()->SetTitle("Waveform");
  }
  else if(flag==1)
  {
    Double_t x[length];
    Double_t y[length];

    for(Short_t i=0; i<length; ++i)
    {
      x[i]=i;
      y[i]=strap[i];
    }

    gr.push_back(NULL);
    gr.back() = new TGraph(length,x,y);
    gr.back()->SetTitle("Short Trapezoid (RT 40 ns, FT 16 ns)");
  }
  else
  {
    Double_t x[length];
    Double_t y[length];

    for(Short_t i=0; i<length; ++i)
    {
      x[i]=i;
      y[i]=ltrap[i];
    }

    gr.push_back(NULL);
    gr.back() = new TGraph(length,x,y);
    gr.back()->SetTitle("Long Trapezoid (RT 1200 ns, FT 400 ns)");
  }

  gr.back()->GetHistogram()->GetXaxis()->SetTitle("Time (4 ns)");
  gr.back()->GetHistogram()->GetYaxis()->SetTitle("Energy (ADC)");
  gr.back()->GetHistogram()->GetXaxis()->SetTitleOffset(1.2);
  gr.back()->GetHistogram()->GetYaxis()->SetTitleOffset(1.4);
  gr.back()->Draw();
}

//-------------------------------------------------------------------------------------------------

void replay::clear_graphs()
{
  for(UInt_t i=0; i<gr.size(); ++i)
    delete gr[i];

  for(UInt_t i=0; i<can.size(); ++i)
    if(gROOT->GetListOfCanvases()->FindObject(can[i]))
      delete can[i];

  can.clear();
  gr.clear();
}

//-------------------------------------------------------------------------------------------------

Byte_t replay::pile_ident()
{
  Short_t x[3];
  Byte_t pks[2];
  Byte_t flag=0;
  Short_t mxwf=0;
  Short_t mywf=data[0];
  Double_t pwidth;
  Double_t wgain;

  for(Short_t i=1; i<length; ++i)
    if(data[i]>mywf)
    {
      mxwf=i;
      mywf=data[i];
    }

  for(Short_t i=0; i<5; ++i)
  {
    mywf+=data[mxwf-i-1];
    mywf+=data[mxwf+i+1];
  }

  mywf/=11;

  find_width(x);
  find_peaks(pks);

  if(x[2]==0)
  {
    pwidth=.00000000000000744876*mywf*mywf*mywf*mywf*mywf-.0000000000457534*mywf*mywf*mywf*mywf
          +.000000109271*mywf*mywf*mywf-0.000129052*mywf*mywf+0.0823380*mywf+7.29708;

    wgain=.0000000000000000420374*mywf*mywf*mywf*mywf*mywf-.000000000000103642*mywf*mywf*mywf*mywf
         -.000000000202988*mywf*mywf*mywf+0.000000284053*mywf*mywf+0.000835234*mywf+9.33095;

    if(x[1]-x[0]>pwidth+wgain)
      flag=1;
  }

  if(pks[1]>0)
    flag=2;

  return flag;
}

//-------------------------------------------------------------------------------------------------

void replay::find_width(Short_t x[3])
{
  Short_t mx;
  Double_t my;
  UShort_t cnt=0;
  Byte_t flag=0;

  mx=0; my=strap[0];

  for(Short_t i=1; i<length; ++i)
    if(strap[i]>my)
    {
      mx=i;
      my=strap[i];
    }

  x[0]=0; x[1]=0; x[2]=0;

  for(Short_t i=0; i<length; ++i)
    if(cnt<12)
    {
      if(strap[i]<60.)
      {
        x[0]=i;
        cnt=0;
      }
      else
        ++cnt;
    }
    else
    {
      if(strap[i]<60.)
      {
        if(i>mx)
        {
          x[1]=i;
          i=length;
          flag=1;
        }
        else
          cnt=0;
      }
      else
        ++cnt;
    }

  if(flag==0)
  {
    x[0]=0;
    x[1]=0;
  }

  if(x[0]==0 || x[1]==0)
    x[2]=1;
}

//-------------------------------------------------------------------------------------------------

void replay::find_peaks(Byte_t pks[2])
{
  pks[0]=0; pks[1]=0;

  for(Short_t i=50; i<length; ++i)
  {
    if(pks[0]==0 && strap[i]>90. && strap[i-1]<strap[i] && strap[i-2]<strap[i-1]
       && strap[i-3]<strap[i-2] && strap[i-4]<strap[i-3] && strap[i-5]<strap[i-4]
       && strap[i-6]<strap[i-5] && strap[i-7]<strap[i-6] && strap[i-8]<strap[i-7])
    {
      pks[0]=1;
      peakSep=i;
      pkf[0]=i;
    }

    if(pks[0]==1 && strap[i]<strap[i-1] && strap[i-1]<strap[i-2] && strap[i-2]<strap[i-3]
       && strap[i-3]<strap[i-4])
      pks[0]=2;

    if(pks[0]==2 && strap[i]>90. && strap[i-1]<strap[i] && strap[i-2]<strap[i-1]
       && strap[i-3]<strap[i-2] && strap[i-4]<strap[i-3] && strap[i-5]<strap[i-4]
       && strap[i-6]<strap[i-5] && strap[i-7]<strap[i-6] && strap[i-8]<strap[i-7])
    {
      pks[0]=3; pks[1]=1;
      peakSep=i-peakSep;
      pkf[1]=i;
    }
  }

  if(pks[1]==0)
    peakSep=0;
}

//-------------------------------------------------------------------------------------------------

Byte_t replay::mpix_check()
{
  Byte_t flag=0;
  ULong64_t ttemp;
  Long64_t etemp;

  twindow[0].push_back(timR);
  twindow[1].push_back(entry);

  if(twindow[0].size()>1 && twindow[0].back()-twindow[0][0]<250
     && twindow[1].back()!=twindow[1][0])
    flag=1;

  if(twindow[0].size()>1 && flag==0)
  {
    ttemp = twindow[0].back();
    etemp = twindow[1].back();

    twindow[0].clear();
    twindow[1].clear();

    twindow[0].push_back(ttemp);
    twindow[1].push_back(etemp);
  }

  return flag;
}

//-------------------------------------------------------------------------------------------------

Byte_t replay::classify()
{
  Byte_t flag=0;

  pileF.push_back(pile_ident());
  mpixF.push_back(mpix_check());

  for(UInt_t i=0; i<mpixF.size(); ++i)
    if(mpixF[i]==0)
    {
      if(mpixF.size()==1)
        flag=1;

      if(mpixF.size()==2 && i==1)
        flag=1;

      if(i>1)
        flag=3;
    }

  if(mpixF.size()>1 && entry==nentries)
    flag=3;

  for(UInt_t i=0; i<pileF.size()-1; ++i)
    if(pileF[i]>0 && flag==3)
      ++flag;

  if(pileF.back()>0 && flag==1)
    ++flag;

  return flag;
}

//-------------------------------------------------------------------------------------------------

void replay::engy_construct(Byte_t flag)
{
  Double_t engyTemp=0.;
  Double_t engyTemp2=0.;
  Int_t cnt=0;
  Int_t cnt2=0;
  Byte_t ptemp;
  Byte_t mtemp;
  Short_t maxTmp1=0, maxTmp2=0;
  Byte_t noiseFlag=0;

  switch(flag)
  {
    case 1:
      if((gCnt[fCnt]!=0) ? (classType[fCnt][gCnt[fCnt]-1]<40) : (1))
      {
        long_trap();
        engyTemp=lfilt_extract();

        if(engyTemp!=-1.)
        {
          engy_spec[fCnt][gCnt[fCnt]]=calib[bd][ch][0]+engyTemp*calib[bd][ch][1]+engyTemp*engyTemp*calib[bd][ch][2];
          classType[fCnt][gCnt[fCnt]]=flag;
          bdchf[fCnt][gCnt[fCnt]]=8*bd+ch;
          ++gCnt[fCnt];
        }
        else
        {
          engy_spec[fCnt][gCnt[fCnt]]=0.;
          classType[fCnt][gCnt[fCnt]]=42;
          bdchf[fCnt][gCnt[fCnt]]=8*bd+ch;
          ++gCnt[fCnt];
        }

        if(mpixF.size()>1)
        {
          ptemp = pileF.back();
          mtemp = mpixF.back();

          pileF.clear();
          mpixF.clear();

          pileF.push_back(ptemp);
          mpixF.push_back(mtemp);
        }
      }

      break;

    case 2:
      if((gCnt[fCnt]!=0) ? (classType[fCnt][gCnt[fCnt]-1]<40) : (1))
      {
        if(peakSep<35)
        {
          long_trap();
          engyTemp=lfilt_extract();

          if(engyTemp!=-1.)
          {
            engy_spec[fCnt][gCnt[fCnt]]=calib[bd][ch][0]+engyTemp*calib[bd][ch][1]+engyTemp*engyTemp*calib[bd][ch][2];
            classType[fCnt][gCnt[fCnt]]=flag;
            bdchf[fCnt][gCnt[fCnt]]=8*bd+ch;
            ++gCnt[fCnt];
          }
          else
          {
            engy_spec[fCnt][gCnt[fCnt]]=0.;
            classType[fCnt][gCnt[fCnt]]=42;
            bdchf[fCnt][gCnt[fCnt]]=8*bd+ch;
            ++gCnt[fCnt];
          }
        }
        else
        {
          sepPile(&maxTmp1,&maxTmp2);
          long_trap();
          engyTemp=lfilt_extract();

          if(engyTemp>.65*(maxTmp1+maxTmp2))
            engyTemp=maxTmp1+maxTmp2;
          else
            noiseFlag=1;

          engy_spec[fCnt][gCnt[fCnt]]=calib[bd][ch][0]+engyTemp*calib[bd][ch][1]+engyTemp*engyTemp*calib[bd][ch][2];
          bdchf[fCnt][gCnt[fCnt]]=8*bd+ch;

          if(noiseFlag==0)
            classType[fCnt][gCnt[fCnt]]=flag+1;
          else
            classType[fCnt][gCnt[fCnt]]=20;

          ++gCnt[fCnt];
        }

        if(mpixF.size()>1)
        {
          ptemp = pileF.back();
          mtemp = mpixF.back();

          pileF.clear();
          mpixF.clear();

          pileF.push_back(ptemp);
          mpixF.push_back(mtemp);
        }
      }

      break;

    case 3:
      for(UInt_t i=1; i<mpixF.size(); ++i)
      {
        read(-1,1);
        if(classType[fCnt][gCnt[fCnt]-i]<40 || classType[fCnt][gCnt[fCnt]-i]>41)
        {
          sub_base();
          long_trap();
          if((engyTemp2=lfilt_extract())!=-1.)
          {
            engyTemp+=calib[bd][ch][0]+engyTemp2*calib[bd][ch][1]+engyTemp2*engyTemp2*calib[bd][ch][2];
            ++cnt;
          }
        }
      }

      engy_spec[fCnt][gCnt[fCnt]-1] = engyTemp;
      bdchf[fCnt][gCnt[fCnt]-1]=8*bd+ch;

      if(cnt==Int_t(mpixF.size()-1))
        classType[fCnt][gCnt[fCnt]-1] = flag+1;
      else
        classType[fCnt][gCnt[fCnt]-1] = flag+2;

      if(entry!=nentries)
        read(mpixF.size()-2,1);
      else
        read(mpixF.size()-1,1);

      ptemp = pileF.back();
      mtemp = mpixF.back();

      pileF.clear();
      mpixF.clear();

      pileF.push_back(ptemp);
      mpixF.push_back(mtemp);
      break;

    case 4:
      for(UInt_t i=1; i<mpixF.size(); ++i)
      {
        read(-1,1);
        if(classType[fCnt][gCnt[fCnt]-i]<40 || classType[fCnt][gCnt[fCnt]-i]>41)
        {
          sub_base();
          short_trap();
          if(pile_ident()>0)
          {
            if(peakSep<35)
            {
              long_trap();

              if((engyTemp2=lfilt_extract())!=-1.)
                engyTemp+=calib[bd][ch][0]+engyTemp2*calib[bd][ch][1]+engyTemp2*engyTemp2*calib[bd][ch][2];
              else
                --cnt;

              ++cnt2;
            }
            else
            {
              sepPile(&maxTmp1,&maxTmp2);
              long_trap();
              engyTemp2=maxTmp1+maxTmp2;
              if(lfilt_extract()>.65*engyTemp2)
                engyTemp+=calib[bd][ch][0]+engyTemp2*calib[bd][ch][1]+engyTemp2*engyTemp2*calib[bd][ch][2];
              else
                noiseFlag=2;
            }
          }
          else
          {
            long_trap();

            if((engyTemp2=lfilt_extract())!=-1.)
              engyTemp+=calib[bd][ch][0]+engyTemp2*calib[bd][ch][1]+engyTemp2*engyTemp2*calib[bd][ch][2];
            else
              --cnt;
          }
          ++cnt;
        }
      }

      engy_spec[fCnt][gCnt[fCnt]-1] = engyTemp;
      bdchf[fCnt][gCnt[fCnt]-1]=8*bd+ch;
      if(noiseFlag==0)
      {
        if(cnt==Int_t(mpixF.size()-1))
        {
          if(cnt2==Int_t(mpixF.size()-1))
            classType[fCnt][gCnt[fCnt]-1] = flag+2;
          else
            classType[fCnt][gCnt[fCnt]-1] = flag+3;
        }
        else
          if(cnt2==Int_t(mpixF.size()-1))
            classType[fCnt][gCnt[fCnt]-1] = flag+4;
          else
            classType[fCnt][gCnt[fCnt]-1] = flag+5;
      }
      else
        classType[fCnt][gCnt[fCnt]-1]=21;

      if(entry!=nentries)
        read(mpixF.size()-2,1);
      else
        read(mpixF.size()-1,1);

      ptemp = pileF.back();
      mtemp = mpixF.back();

      pileF.clear();
      mpixF.clear();

      pileF.push_back(ptemp);
      mpixF.push_back(mtemp);
  }

  if(((engy_spec[fCnt][gCnt[fCnt]-1]>256. && clbFlag==1) || (engy_spec[fCnt][gCnt[fCnt]-1]>1400.
     && clbFlag==0) || peakSep>65) && classType[fCnt][gCnt[fCnt]-1]>0 && classType[fCnt][gCnt[fCnt]-1]<40)
    classType[fCnt][gCnt[fCnt]-1]+=10;
}

//-------------------------------------------------------------------------------------------------

Double_t replay::lfilt_extract()
{
  Short_t mxtrap=0;
  Double_t mytrap=ltrap[0];
  Short_t corner[2];

  corner[0]=0; corner[1]=0;

  for(Short_t i=1; i<length; ++i)
    if(ltrap[i]>mytrap)
    {
      mxtrap=i;
      mytrap=ltrap[i];
    }

  for(Short_t i=0,j=0; i<LFT+200; ++i)
  {
    if(j==0)
      for(Short_t k=1; k<6; ++k)
      {
        if(ltrap[900+LRT+i]>ltrap[900+LRT+i+k]-k*mytrap/(2*LRT))
          ++j;

        if(k==5)
        {
          if(j==5)
            corner[0]=900+LRT+i;
          else
            j=0;
        }
      }
    else
      for(Short_t k=1; k<6; ++k)
      {
        if(ltrap[900+LRT+i]<ltrap[900+LRT+i-k]-k*mytrap/(2*LRT))
          ++j;

        if(k==5)
        {
          if(j==10)
          {
            corner[1]=900+LRT+i-5;
            i=LFT+200;
          }
          else
            j=5;
        }
      }
  }

  if(corner[0]!=0 && corner[1]!=0)
  {
    mytrap=(ltrap[corner[0]+2*(corner[1]-corner[0])/3]+9.473)/1.1061;
    return mytrap;
  }
  else
    return -1.;
}

//-------------------------------------------------------------------------------------------------

void replay::sepPile(Short_t* maxTmp1,Short_t* maxTmp2)
{
  Short_t minx=pkf[1]-9;
  Short_t miny=data[minx];
  Short_t max1x, max1y, max2x, max2y;
  Short_t htDif;

  for(Short_t i=0; i<2; ++i)
  {
    miny+=data[minx-i-1];
    miny+=data[minx+i+1];
  }

  miny/=5;

  max1x=0; max1y=data[0];

  for(Short_t i=1; i<minx; ++i)
    if(data[i]>max1y)
    {
      max1x=i;
      max1y=data[i];
    }

  for(Short_t i=0; i<5; ++i)
  {
    max1y+=data[max1x-i-1];
    max1y+=data[max1x+i+1];
  }

  max1y/=11;

  max2x=minx; max2y=data[minx];

  for(Short_t i=minx+1; i<length; ++i)
    if(data[i]>max2y)
    {
      max2x=i;
      max2y=data[i];
    }

  for(Short_t i=0; i<5; ++i)
  {
    max2y+=data[max2x-i-1];
    max2y+=data[max2x+i+1];
  }

  max2y/=11;

  htDif = max2y - miny*exp((Double_t)(minx-max2x)/dec[bd][ch]);

  *maxTmp1 = max1y; *maxTmp2 = htDif;
}

//-------------------------------------------------------------------------------------------------

void replay::plot_main(Byte_t bdN, Byte_t chN)
{
  string stemp = "bd " + to_string(bdN) + ", ch " + to_string(chN) + ": Energy Spectrum";

  gui->maintab(0);

  if(histE[0]!=NULL)
    delete histE[0];

  if(clbFlag==0)
  {
    if(bdN<6)
      histE[0] = new TH1D("ehistt",stemp.c_str(),1400,0,7000);
    else
      histE[0] = new TH1D("ehistt","Total Energy Spectrum",1400,0,7000);

    histE[0]->GetXaxis()->SetTitle("Energy (ADC)");
  }
  else
  {
    if(bdN<6)
      histE[0] = new TH1D("ehistt",stemp.c_str(),320,0,320);
    else
      histE[0] = new TH1D("ehistt","Total Energy Spectrum",320,0,320);

    histE[0]->GetXaxis()->SetTitle("Energy (keV)");
  }

  histE[0]->GetYaxis()->SetTitle("Count");
  histE[0]->GetXaxis()->SetTitleOffset(1.2);
  histE[0]->GetYaxis()->SetTitleOffset(1.4);

  if(bdN<6)
  {
    for(Int_t i=0; i<fNum; ++i)
      for(Long64_t j=0; j<gCnt[i]; ++j)
        if(((classType[i][j]>0 && classType[i][j]<5) || classType[i][j]==6 || classType[i][j]==7 ||
           (classType[i][j]>10 && classType[i][j]<15) || classType[i][j]==16 || classType[i][j]==17)
           && bdchf[i][j]==8*bdN+chN)
          histE[0]->Fill(engy_spec[i][j]);
  }
  else
    for(Int_t i=0; i<fNum; ++i)
      for(Long64_t j=0; j<gCnt[i]; ++j)
        if((classType[i][j]>0 && classType[i][j]<5) || classType[i][j]==6 || classType[i][j]==7 ||
           (classType[i][j]>10 && classType[i][j]<15) || classType[i][j]==16 || classType[i][j]==17)
          histE[0]->Fill(engy_spec[i][j]);

  histE[0]->Draw();

  gui->maintab(1);

  gPad->SetLogy();
  histE[0]->Draw();
}

//-------------------------------------------------------------------------------------------------

void replay::plot_stats(Byte_t bdN, Byte_t chN)
{
  Long64_t cnt[25];

  Long64_t nentriesTot=0;

  for(Int_t i=0; i<fNum; ++i)
    nentriesTot+=nentriesT[i];

  for(Short_t i=0; i<25; ++i)
    cnt[i]=0;

  for(Int_t i=0; i<fNum; ++i)
    for(Long64_t j=0; j<gCnt[i]; ++j)
      if((bdN<6) ? (bdchf[i][j]==8*bdN+chN) : (1))
        switch(classType[i][j])
        {
          case 1:
            ++cnt[5];
            break;

          case 2:
            ++cnt[6];
            break;

          case 3:
            ++cnt[7];
            break;

          case 4:
            ++cnt[8];
            break;

          case 5:
            ++cnt[9];
            break;

          case 6:
            ++cnt[10];
            break;

          case 7:
            ++cnt[11];
            break;

          case 8:
          case 9:
            ++cnt[12];
            break;

          case 11:
            ++cnt[13];
            break;

          case 12:
            ++cnt[14];
            break;

          case 13:
            ++cnt[15];
            break;

          case 14:
            ++cnt[16];
            break;

          case 15:
            ++cnt[17];
            break;

          case 16:
            ++cnt[18];
            break;

          case 17:
            ++cnt[19];
            break;

          case 18:
          case 19:
            ++cnt[20];
            break;

          case 20:
            ++cnt[21];
            break;

          case 21:
            ++cnt[22];
            break;

          case 30:
            ++cnt[23];

          case 31:
            ++cnt[24];

          case 40:
            ++cnt[2];
            break;

          case 41:
            ++cnt[3];
            break;

          case 42:
            ++cnt[4];
            break;
        }

  if(statInitFlag==0)
  {
    cout << "Decay Values/Counts:\n";

    for(Short_t i=0; i<6; ++i)
    {
      cout << "Bd " << i << ":\n";

      for(Short_t j=0; j<8; ++j)
      {
        cout << "Ch " << j << ": " << dec[i][j] << "/" << decCnt[i][j];

        if(j<7)
          cout << ", ";
        else
          cout << '\n';
      }
    }

    cout << "\n# of Entries: " << nentriesTot << '\n';

    for(Short_t i=0; i<25; ++i)
      cout << "Count " << i << ": " << cnt[i] << '\n';

    statInitFlag=1;
  }

  gui->maintab(2);

	TPaveText* tex = new TPaveText(0.05,0.95,0.95,0.05,"NDC");
	tex->SetBorderSize(0);
	tex->SetFillColor(0);
	tex->SetTextSize(0.055);
	tex->SetTextFont(42);
	tex->SetTextAlign(12);

	Char_t tempstr[80] = {' '};

  sprintf(tempstr,"Number of Entries: %lld",nentriesTot);
  tex->AddText(tempstr);
  sprintf(tempstr,"Number of Good Events: %lld",cnt[5]+cnt[6]+cnt[7]+cnt[8]+cnt[10]+cnt[11]
          +cnt[13]+cnt[14]+cnt[15]+cnt[16]+cnt[18]+cnt[19]);
  tex->AddText(tempstr);

	tex->AddText("Type Counts:");
	sprintf(tempstr,"  Single Pix., No Pile: %lld",cnt[5]+cnt[13]);
	tex->AddText(tempstr);
	sprintf(tempstr,"  Single Pix., With Pile: %lld",cnt[6]+cnt[7]+cnt[14]+cnt[15]);
	tex->AddText(tempstr);
	sprintf(tempstr,"  Multi. Pix., No Pile: %lld",cnt[8]+cnt[16]);
	tex->AddText(tempstr);
	sprintf(tempstr,"  Multi. Pix., With Pile: %lld",cnt[10]+cnt[11]+cnt[18]+cnt[19]);
	tex->AddText(tempstr);

	sprintf(tempstr,"Est. Accidental Pile-up: %lld",cnt[14]+cnt[15]);
	tex->AddText(tempstr);

	tex->AddText("Error Counts:");
	sprintf(tempstr,"  (40) Channel 0: %lld",cnt[2]);
	tex->AddText(tempstr);
	sprintf(tempstr,"  (41) Result 2: %lld",cnt[3]);
	tex->AddText(tempstr);
	sprintf(tempstr,"  (42) No Corners: %lld",cnt[4]);
	tex->AddText(tempstr);
	sprintf(tempstr,"  Mult. Pix., No Pile (w/ 42): %lld",cnt[9]);
	tex->AddText(tempstr);
	sprintf(tempstr,"  Mult. Pix., With Pile (w/ 42): %lld",cnt[12]);
	tex->AddText(tempstr);

	tex->Draw();
}

//-------------------------------------------------------------------------------------------------

void replay::plot_energy(Byte_t bdN, Byte_t chN)
{
  string stemp = "bd " + to_string(bdN) + ", ch " + to_string(chN)
               + ": Single Pixel, No Pile-Up, Energy Spectrum";

	gui->set0(1);

  if(histE[1]!=NULL)
    delete histE[1];

  if(clbFlag==0)
  {
    if(bdN<6)
      histE[1] = new TH1D("ehist1",stemp.c_str(),1400,0,7000);
    else
      histE[1] = new TH1D("ehist1","Total Single Pixel, No Pile-Up, Energy Spectrum",1400,0,7000);

    histE[1]->GetXaxis()->SetTitle("Energy (ADC)");
  }
  else
  {
    if(bdN<6)
      histE[1] = new TH1D("ehist1",stemp.c_str(),320,0,320);
    else
      histE[1] = new TH1D("ehist1","Total Single Pixel, No Pile-Up, Energy Spectrum",320,0,320);

    histE[1]->GetXaxis()->SetTitle("Energy (keV)");
  }

  histE[1]->GetYaxis()->SetTitle("Count");
  histE[1]->GetXaxis()->SetTitleOffset(1.2);
  histE[1]->GetYaxis()->SetTitleOffset(1.4);

  if(bdN<6)
  {
    for(Int_t i=0; i<fNum; ++i)
      for(Long64_t j=0; j<gCnt[i]; ++j)
        if((classType[i][j]==1 || classType[i][j]==11) && bdchf[i][j]==8*bdN+chN)
          histE[1]->Fill(engy_spec[i][j]);
  }
  else
    for(Int_t i=0; i<fNum; ++i)
      for(Long64_t j=0; j<gCnt[i]; ++j)
        if(classType[i][j]==1 || classType[i][j]==11)
          histE[1]->Fill(engy_spec[i][j]);

  histE[1]->Draw();

  gui->set1(1);

  gPad->SetLogy();
  histE[1]->Draw();

  stemp = "bd " + to_string(bdN) + ", ch " + to_string(chN)
               + ": Single Pixel, With Pile-Up, Energy Spectrum";

	gui->set0(2);

  if(histE[2]!=NULL)
    delete histE[2];

  if(clbFlag==0)
  {
    if(bdN<6)
      histE[2] = new TH1D("ehist2",stemp.c_str(),1400,0,7000);
    else
      histE[2] = new TH1D("ehist2","Total Single Pixel, With Pile-Up, Energy Spectrum",1400,0,7000);

    histE[2]->GetXaxis()->SetTitle("Energy (ADC)");
  }
  else
  {
    if(bdN<6)
      histE[2] = new TH1D("ehist2",stemp.c_str(),320,0,320);
    else
      histE[2] = new TH1D("ehist2","Total Single Pixel, With Pile-Up, Energy Spectrum",320,0,320);

    histE[2]->GetXaxis()->SetTitle("Energy (keV)");
  }

  histE[2]->GetYaxis()->SetTitle("Count");
  histE[2]->GetXaxis()->SetTitleOffset(1.2);
  histE[2]->GetYaxis()->SetTitleOffset(1.4);

  if(bdN<6)
  {
    for(Int_t i=0; i<fNum; ++i)
      for(Long64_t j=0; j<gCnt[i]; ++j)
        if((classType[i][j]==2 || classType[i][j]==3 || classType[i][j]==12 || classType[i][j]==13)
           && bdchf[i][j]==8*bdN+chN)
          histE[2]->Fill(engy_spec[i][j]);
  }
  else
    for(Int_t i=0; i<fNum; ++i)
      for(Long64_t j=0; j<gCnt[i]; ++j)
        if(classType[i][j]==2 || classType[i][j]==3 || classType[i][j]==12 || classType[i][j]==13)
          histE[2]->Fill(engy_spec[i][j]);

  histE[2]->Draw();

  gui->set1(2);

  gPad->SetLogy();
  histE[2]->Draw();

  stemp = "bd " + to_string(bdN) + ", ch " + to_string(chN)
               + ": Multiple Pixels, No Pile-Up, Energy Spectrum";

	gui->set0(3);

  if(histE[3]!=NULL)
    delete histE[3];

  if(clbFlag==0)
  {
    if(bdN<6)
      histE[3] = new TH1D("ehist3",stemp.c_str(),1400,0,7000);
    else
      histE[3] = new TH1D("ehist3","Total Multiple Pixels, No Pile-Up, Energy Spectrum",1400,0,7000);

    histE[3]->GetXaxis()->SetTitle("Energy (ADC)");
  }
  else
  {
    if(bdN<6)
      histE[3] = new TH1D("ehist3",stemp.c_str(),320,0,320);
    else
      histE[3] = new TH1D("ehist3","Total Multiple Pixels, No Pile-Up, Energy Spectrum",320,0,320);

    histE[3]->GetXaxis()->SetTitle("Energy (keV)");
  }

  histE[3]->GetYaxis()->SetTitle("Count");
  histE[3]->GetXaxis()->SetTitleOffset(1.2);
  histE[3]->GetYaxis()->SetTitleOffset(1.4);

  if(bdN<6)
  {
    for(Int_t i=0; i<fNum; ++i)
      for(Long64_t j=0; j<gCnt[i]; ++j)
        if((classType[i][j]==4 || classType[i][j]==14) && bdchf[i][j]==8*bdN+chN)
          histE[3]->Fill(engy_spec[i][j]);
  }
  else
    for(Int_t i=0; i<fNum; ++i)
      for(Long64_t j=0; j<gCnt[i]; ++j)
        if(classType[i][j]==4 || classType[i][j]==14)
          histE[3]->Fill(engy_spec[i][j]);

  histE[3]->Draw();

  gui->set1(3);

  gPad->SetLogy();
  histE[3]->Draw();

  stemp = "bd " + to_string(bdN) + ", ch " + to_string(chN)
               + ": Multiple Pixels, With Pile-Up, Energy Spectrum";

	gui->set0(4);

  if(histE[4]!=NULL)
    delete histE[4];

  if(clbFlag==0)
  {
    if(bdN<6)
      histE[4] = new TH1D("ehist4",stemp.c_str(),1400,0,7000);
    else
      histE[4] = new TH1D("ehist4","Total Multiple Pixels, With Pile-Up, Energy Spectrum",1400,0,7000);

    histE[4]->GetXaxis()->SetTitle("Energy (ADC)");
  }
  else
  {
    if(bdN<6)
      histE[4] = new TH1D("ehist4",stemp.c_str(),320,0,320);
    else
      histE[4] = new TH1D("ehist4","Total Multiple Pixels, With Pile-Up, Energy Spectrum",320,0,320);

    histE[4]->GetXaxis()->SetTitle("Energy (keV)");
  }

  histE[4]->GetYaxis()->SetTitle("Count");
  histE[4]->GetXaxis()->SetTitleOffset(1.2);
  histE[4]->GetYaxis()->SetTitleOffset(1.4);

  if(bdN<6)
  {
    for(Int_t i=0; i<fNum; ++i)
      for(Long64_t j=0; j<gCnt[i]; ++j)
        if((classType[i][j]==6 || classType[i][j]==7 || classType[i][j]==16 || classType[i][j]==17)
           && bdchf[i][j]==8*bdN+chN)
          histE[4]->Fill(engy_spec[i][j]);
  }
  else
    for(Int_t i=0; i<fNum; ++i)
      for(Long64_t j=0; j<gCnt[i]; ++j)
        if(classType[i][j]==6 || classType[i][j]==7 || classType[i][j]==16 || classType[i][j]==17)
          histE[4]->Fill(engy_spec[i][j]);

  histE[4]->Draw();

  gui->set1(4);

  gPad->SetLogy();
  histE[4]->Draw();
}

//-------------------------------------------------------------------------------------------------

void replay::plot_pixels()
{
  gui->set2(1);
  hist = new TH1D("phist","Pixel Count",48,0,48);
  hist->GetXaxis()->SetTitle("Pixel Number (8*bd+ch)");
  hist->GetYaxis()->SetTitle("Count");
  hist->GetXaxis()->SetTitleOffset(1.2);
  hist->GetYaxis()->SetTitleOffset(1.4);

  for(Int_t i=0; i<fNum; ++i)
    for(Long64_t j=0; j<nentriesT[i]; ++j)
      hist->Fill(8*bdch[i][0][j]+bdch[i][1][j]);

  hist->Draw();

  gui->set2(2);

  gPad->SetLogy();
  hist->Draw();
}

//-------------------------------------------------------------------------------------------------

void replay::plot_pixelsTime()
{
  Long64_t nentriesTot=0;

  for(Int_t i=0; i<fNum; ++i)
    nentriesTot+=nentriesT[i];

  Double_t *x = new Double_t[nentriesTot];
  Double_t *y = new Double_t[nentriesTot];

  nentriesTot=0;

  for(Int_t i=0; i<fNum; ++i)
  {
    for(Long64_t j=0; j<nentriesT[i]; ++j)
    {
      x[nentriesTot+j]=timWF[i][j]/250;
      y[nentriesTot+j]=8*bdch[i][0][j]+bdch[i][1][j];
    }

    nentriesTot+=nentriesT[i];
  }

  gui->set3();
  gr2[6] = new TGraph(nentriesTot,x,y);
  gr2[6]->SetTitle("Pixel vs. Time");
  gr2[6]->GetHistogram()->GetXaxis()->SetTitle("Time (us)");
  gr2[6]->GetHistogram()->GetYaxis()->SetTitle("Pixel Number (8*bd+ch)");
  gr2[6]->GetHistogram()->GetXaxis()->SetTitleOffset(1.2);
  gr2[6]->GetHistogram()->GetYaxis()->SetTitleOffset(1.4);
  gr2[6]->SetMarkerStyle(7);
  gr2[6]->Draw("Ap0");

  delete[] x;
  delete[] y;
}

//-------------------------------------------------------------------------------------------------

void replay::plotWF(Long64_t n, Byte_t flag)
{
  Int_t x[length];
  Int_t y[length];
  string stemp;

  if(flag==0)
  {
	  TPaveText* tex = new TPaveText(0.05,0.95,0.99,0.01,"NDC");
	  tex->SetBorderSize(0);
	  tex->SetFillColor(0);
	  tex->SetTextSize(0.12);
	  tex->SetTextFont(42);
	  tex->SetTextAlign(11);

	  Char_t tempstr[30] = {' '};

	  sprintf(tempstr,"Board:  %u", bd);
	  tex->AddText(tempstr);
	  sprintf(tempstr,"Channel:  %u", ch);
	  tex->AddText(tempstr);
    tex->AddText("Timestamp:");
	  sprintf(tempstr,"   %E ms", (Double_t)timR/250000);
	  tex->AddText(tempstr);


    gui->statsWFc();
	  tex->Draw();

    gui->statsWFu();
    gui->selCanWF();
  }

  if(flag==0)
    stemp = "WF: " + to_string(n);
  else if(flag==1)
    stemp = "Long Trapezoid Filter: " + to_string(n);
  else
    stemp = "Short Trapezoid Filter: " + to_string(n);

  for(Short_t i=0; i<length; ++i)
  {
    x[i]=i;
    if(flag==0)
      y[i]=data[i];
    else if(flag==1)
      y[i]=(ltrap[i]+9.473)/1.1061;
    else
      y[i]=strap[i];
  }

  if(grWF!=NULL)
    delete grWF;

  grWF = new TGraph(length,x,y);
  grWF->SetTitle(stemp.c_str());
  grWF->GetHistogram()->GetXaxis()->SetTitle("Time (4 ns)");

  if(clbFlag==0)
    grWF->GetHistogram()->GetYaxis()->SetTitle("Energy (ADC)");
  else
    grWF->GetHistogram()->GetYaxis()->SetTitle("Energy (keV)");

  grWF->GetHistogram()->GetXaxis()->SetTitleOffset(1.2);
  grWF->GetHistogram()->GetYaxis()->SetTitleOffset(1.4);
  grWF->Draw();
}

//-------------------------------------------------------------------------------------------------

void replay::plotRTime()
{
  gui->set0b(1);
  hist = new TH1D("rt1hist","Rise Time Distribution",200,0,500);
  hist->GetXaxis()->SetTitle("Time (ns)");
  hist->GetYaxis()->SetTitle("Count");
  hist->GetXaxis()->SetTitleOffset(1.2);
  hist->GetYaxis()->SetTitleOffset(1.4);
  gPad->SetLogy();

  for(Long64_t i=0; i<nentries2; ++i)
    hist->Fill(rtime[i]*4);

  hist->Draw();

  gui->set0b(2);
  hist = new TH1D("rt2hist","Rise Time Distribution",200,0,200);
  hist->GetXaxis()->SetTitle("Time (ns)");
  hist->GetYaxis()->SetTitle("Count");
  hist->GetXaxis()->SetTitleOffset(1.2);
  hist->GetYaxis()->SetTitleOffset(1.4);
  gPad->SetLogy();

  for(Long64_t i=0; i<nentries2; ++i)
    hist->Fill(rtime[i]*4);

  hist->Draw();
}

//-------------------------------------------------------------------------------------------------

void replay::plotFTime()
{
  gui->set1b();
  hist = new TH1D("fthist","Fall Time Distribution",200,2000,6000);
  hist->GetXaxis()->SetTitle("Time (ns)");
  hist->GetYaxis()->SetTitle("Count");
  hist->GetXaxis()->SetTitleOffset(1.2);
  hist->GetYaxis()->SetTitleOffset(1.4);
  gPad->SetLogy();

  for(Long64_t i=0; i<nentries2; ++i)
    hist->Fill(ftime[i]*4);

  hist->Draw();
}

//-------------------------------------------------------------------------------------------------

void replay::plott0()
{
  gui->set2b();
  hist = new TH1D("t0hist","t_0 Distribution",200,0,5000);
  hist->GetXaxis()->SetTitle("Time (ns)");
  hist->GetYaxis()->SetTitle("Count");
  hist->GetXaxis()->SetTitleOffset(1.2);
  hist->GetYaxis()->SetTitleOffset(1.4);
  gPad->SetLogy();

  for(Long64_t i=0; i<nentries2; ++i)
    hist->Fill(itime[i]*4);

  hist->Draw();
}

//-------------------------------------------------------------------------------------------------

void replay::plotTemp()
{
  gui->set0c(1);
  hist = new TH1D("tmpHist1","Temp. Sensor 1 Distribution",200,0,100);
  hist->GetXaxis()->SetTitle("Temperature (K)");
  hist->GetYaxis()->SetTitle("Count");
  hist->GetXaxis()->SetTitleOffset(1.2);
  hist->GetYaxis()->SetTitleOffset(1.4);

  for(Long64_t i=0; i<nentries3; ++i)
    hist->Fill(slowData[1][i]);

  hist->Draw();

  gui->set0c(2);
  hist = new TH1D("tmpHist2","Temp. Sensor 2 Distribution",200,0,100);
  hist->GetXaxis()->SetTitle("Temperature (K)");
  hist->GetYaxis()->SetTitle("Count");
  hist->GetXaxis()->SetTitleOffset(1.2);
  hist->GetYaxis()->SetTitleOffset(1.4);

  for(Long64_t i=0; i<nentries3; ++i)
    hist->Fill(slowData[2][i]);

  hist->Draw();

  gui->set0c(3);
  hist = new TH1D("tmpHist3","Temp. Sensor 3 Distribution",200,0,100);
  hist->GetXaxis()->SetTitle("Temperature (K)");
  hist->GetYaxis()->SetTitle("Count");
  hist->GetXaxis()->SetTitleOffset(1.2);
  hist->GetYaxis()->SetTitleOffset(1.4);

  for(Long64_t i=0; i<nentries3; ++i)
    hist->Fill(slowData[3][i]);

  hist->Draw();

  gui->set0c(4);
  hist = new TH1D("tmpHist4","Temp. Sensor 4 Distribution",200,0,100);
  hist->GetXaxis()->SetTitle("Temperature (K)");
  hist->GetYaxis()->SetTitle("Count");
  hist->GetXaxis()->SetTitleOffset(1.2);
  hist->GetYaxis()->SetTitleOffset(1.4);

  for(Long64_t i=0; i<nentries3; ++i)
    hist->Fill(slowData[4][i]);

  hist->Draw();
}

//-------------------------------------------------------------------------------------------------

void replay::plotTempTime()
{
  gui->set1c(1);
  gr2[0] = new TGraph(nentries3,slowData[0],slowData[1]);
  gr2[0]->SetTitle("Temp. Sensor 1 vs. Time");
  gr2[0]->GetHistogram()->GetXaxis()->SetTitle("Time (s)");
  gr2[0]->GetHistogram()->GetYaxis()->SetTitle("Temperature (K)");
  gr2[0]->GetHistogram()->GetXaxis()->SetTitleOffset(1.2);
  gr2[0]->GetHistogram()->GetYaxis()->SetTitleOffset(1.4);
  gr2[0]->SetMarkerStyle(7);
  gr2[0]->Draw("Ap0");

  gui->set1c(2);
  gr2[1] = new TGraph(nentries3,slowData[0],slowData[2]);
  gr2[1]->SetTitle("Temp. Sensor 2 vs. Time");
  gr2[1]->GetHistogram()->GetXaxis()->SetTitle("Time (s)");
  gr2[1]->GetHistogram()->GetYaxis()->SetTitle("Temperature (K)");
  gr2[1]->GetHistogram()->GetXaxis()->SetTitleOffset(1.2);
  gr2[1]->GetHistogram()->GetYaxis()->SetTitleOffset(1.4);
  gr2[1]->SetMarkerStyle(7);
  gr2[1]->Draw("Ap0");

  gui->set1c(3);
  gr2[2] = new TGraph(nentries3,slowData[0],slowData[3]);
  gr2[2]->SetTitle("Temp. Sensor 3 vs. Time");
  gr2[2]->GetHistogram()->GetXaxis()->SetTitle("Time (s)");
  gr2[2]->GetHistogram()->GetYaxis()->SetTitle("Temperature (K)");
  gr2[2]->GetHistogram()->GetXaxis()->SetTitleOffset(1.2);
  gr2[2]->GetHistogram()->GetYaxis()->SetTitleOffset(1.4);
  gr2[2]->SetMarkerStyle(7);
  gr2[2]->Draw("Ap0");

  gui->set1c(4);
  gr2[3] = new TGraph(nentries3,slowData[0],slowData[4]);
  gr2[3]->SetTitle("Temp. Sensor 4 vs. Time");
  gr2[3]->GetHistogram()->GetXaxis()->SetTitle("Time (s)");
  gr2[3]->GetHistogram()->GetYaxis()->SetTitle("Temperature (K)");
  gr2[3]->GetHistogram()->GetXaxis()->SetTitleOffset(1.2);
  gr2[3]->GetHistogram()->GetYaxis()->SetTitleOffset(1.4);
  gr2[3]->SetMarkerStyle(7);
  gr2[3]->Draw("Ap0");
}

//-------------------------------------------------------------------------------------------------

void replay::plotCurr()
{
  gui->set2c(1);
  hist = new TH1D("curHist1","Current Sensor 1 Distribution",200,0,15);
  hist->GetXaxis()->SetTitle("Current (nA)");
  hist->GetYaxis()->SetTitle("Count");
  hist->GetXaxis()->SetTitleOffset(1.2);
  hist->GetYaxis()->SetTitleOffset(1.4);

  for(Long64_t i=0; i<nentries3; ++i)
    hist->Fill(slowData[5][i]);

  hist->Draw();

  gui->set2c(2);
  hist = new TH1D("curHist2","Current Sensor 2 Distribution",200,0,15);
  hist->GetXaxis()->SetTitle("Current (nA)");
  hist->GetYaxis()->SetTitle("Count");
  hist->GetXaxis()->SetTitleOffset(1.2);
  hist->GetYaxis()->SetTitleOffset(1.4);

  for(Long64_t i=0; i<nentries3; ++i)
    hist->Fill(slowData[6][i]);

  hist->Draw();
}

//-------------------------------------------------------------------------------------------------

void replay::plotCurrTime()
{
  gui->set3c(1);
  gr2[4] = new TGraph(nentries3,slowData[0],slowData[5]);
  gr2[4]->SetTitle("Current Sensor 1 vs. Time");
  gr2[4]->GetHistogram()->GetXaxis()->SetTitle("Time (s)");
  gr2[4]->GetHistogram()->GetYaxis()->SetTitle("Current (nA)");
  gr2[4]->GetHistogram()->GetXaxis()->SetTitleOffset(1.2);
  gr2[4]->GetHistogram()->GetYaxis()->SetTitleOffset(1.4);
  gr2[4]->SetMarkerStyle(7);
  gr2[4]->Draw("Ap0");

  gui->set3c(2);
  gr2[5] = new TGraph(nentries3,slowData[0],slowData[6]);
  gr2[5]->SetTitle("Current Sensor 2 vs. Time");
  gr2[5]->GetHistogram()->GetXaxis()->SetTitle("Time (s)");
  gr2[5]->GetHistogram()->GetYaxis()->SetTitle("Current (nA)");
  gr2[5]->GetHistogram()->GetXaxis()->SetTitleOffset(1.2);
  gr2[5]->GetHistogram()->GetYaxis()->SetTitleOffset(1.4);
  gr2[5]->SetMarkerStyle(7);
  gr2[5]->Draw("Ap0");
}

#endif
