


#include "replay.cpp"


replay* replay_script(string fname, Int_t fStart, Int_t fNum, Byte_t clb, Byte_t expf)
{
  if(fStart<0)
  {
    cout << "\n2nd argument must be greater than -1\n\n";
    return NULL;
  }

  if(fNum<1)
  {
    cout << "\n3rd argument must be greater than 0\n\n";
    return NULL;
  }

  Long64_t timeCheck = time(0);  

  Byte_t flag;
  string stemp;

  replay* rep = new replay(fNum);

  if((clb!=0) ? (rep->calibrate()==0) : (1))
  {
    for(Int_t i=0; i<fNum; ++i)
    {
      stemp = fname + "_" + to_string(fStart+i) + ".bin";

      if(rep->openf(stemp)==0)
      {
        if(expf!=0 && i==0)
          rep->exp_fit();

        for(Long64_t j=0; j<rep->nentries;)
        {
          rep->read();

          if(rep->error!=5)
          {
            rep->sub_base();
            rep->short_trap();

            flag = rep->classify();

            if(flag!=0)
              rep->engy_construct(flag);

            j=rep->entry;

            if(flag!=0 && j==rep->nentries-1)
              j=rep->nentries;
          }
          else
          {
            j=rep->nentries;
            cout << "\nResult 0 Found, Run Stopped\n\n";
          }
        }
      }
      else
      {
        delete rep;
        rep = NULL;
        i=fNum;
      }
    }

    if(rep!=NULL)
    {
      rep->plot_main(6,0);
      rep->plot_stats(6,0);
      rep->plot_energy(6,0);
      rep->plot_pixels();
      rep->plot_pixelsTime();
    }
  }
  else
  {
    cout << "\nIncorrect Calibration File\n\n";
    delete rep;
    rep = NULL;
  }

  timeCheck=time(0)-timeCheck;

  cout << "Run Time: " << timeCheck << '\n';

  return rep;
}

//-------------------------------------------------------------------------------------------------

replay* replay_script(string fname1, Int_t fStart, Int_t fNum, string fname2, Byte_t fileFlag, Byte_t clb, Byte_t expf)
{
  if(fStart<0)
  {
    cout << "\n2nd argument must be greater than -1\n\n";
    return NULL;
  }

  if(fNum<1)
  {
    cout << "\n3rd argument must be greater than 0\n\n";
    return NULL;
  }

  Long64_t timeCheck = time(0);  

  Byte_t flag;
  string stemp;

  replay* rep = new replay(fNum);

  if((clb!=0) ? (rep->calibrate()==0) : (1))
  {
    if((fileFlag==0) ? (rep->openf2(fname2)==0) : (rep->openf3(fname2)==0))
    {
      if(fileFlag==0)
      {
        for(Long64_t i=0; i<rep->nentries2; ++i)
          rep->read2();

        rep->getDecConst();
      }
      else
        for(Long64_t i=0; i<rep->nentries3; ++i)
          rep->read3();

      for(Int_t i=0; i<fNum; ++i)
      {
        stemp = fname1 + "_" + to_string(fStart+i) + ".bin";

        if(rep->openf(stemp)==0)
        {
          if(expf!=0 && i==0)
            rep->exp_fit();

          for(Long64_t j=0; j<rep->nentries;)
          {
            rep->read();

            if(rep->error!=5)
            {
              rep->sub_base();
              rep->short_trap();

              flag = rep->classify();

              if(flag!=0)
                rep->engy_construct(flag);

              j=rep->entry;

              if(flag!=0 && j==rep->nentries-1)
                j=rep->nentries;
            }
            else
            {
              j=rep->nentries;
              cout << "\nResult 0 Found, Run Stopped\n\n";
            }
          }
        }
        else
        {
          delete rep;
          rep = NULL;
          i=fNum;
        }
      }
    }
    else
    {
      delete rep;
      rep = NULL;
    }

    if(rep!=NULL)
    {
      rep->plot_main(6,0);
      rep->plot_stats(6,0);
      rep->plot_energy(6,0);
      rep->plot_pixels();
      rep->plot_pixelsTime();

      if(fileFlag==0)
      {
        rep->plotRTime();
        rep->plotFTime();
        rep->plott0();
      }
      else
      {
        rep->plotTemp();
        rep->plotTempTime();
        rep->plotCurr();
        rep->plotCurrTime();
      }
    }
  }
  else
  {
    cout << "\nIncorrect Calibration File\n\n";
    delete rep;
    rep = NULL;
  }

  timeCheck=time(0)-timeCheck;

  cout << "Run Time: " << timeCheck << '\n';

  return rep;
}

//-------------------------------------------------------------------------------------------------

replay* replay_script(string fname1, Int_t fStart, Int_t fNum, string fname2, string fname3, Byte_t clb, Byte_t expf)
{
  if(fStart<0)
  {
    cout << "\n2nd argument must be greater than -1\n\n";
    return NULL;
  }

  if(fNum<1)
  {
    cout << "\n3rd argument must be greater than 0\n\n";
    return NULL;
  }

  Long64_t timeCheck = time(0);  

  Byte_t flag;
  string stemp;

  replay* rep = new replay(fNum);

  if((clb!=0) ? (rep->calibrate()==0) : (1))
  {
    if(rep->openf2(fname2)==0 && rep->openf3(fname3)==0)
    {
      for(Long64_t i=0; i<rep->nentries2; ++i)
        rep->read2();

      rep->getDecConst();

      for(Long64_t i=0; i<rep->nentries3; ++i)
        rep->read3();

      for(Int_t i=0; i<fNum; ++i)
      {
        stemp = fname1 + "_" + to_string(fStart+i) + ".bin";

        if(rep->openf(stemp)==0)
        {
          if(expf!=0 && i==0)
            rep->exp_fit();

          for(Long64_t j=0; j<rep->nentries;)
          {
            rep->read();

            if(rep->error!=5)
            {
              rep->sub_base();
              rep->short_trap();

              flag = rep->classify();

              if(flag!=0)
                rep->engy_construct(flag);

              j=rep->entry;

              if(flag!=0 && j==rep->nentries-1)
                j=rep->nentries;
            }
            else
            {
              j=rep->nentries;
              cout << "\nResult 0 Found, Run Stopped\n\n";
            }
          }
        }
        else
        {
          delete rep;
          rep = NULL;
          i=fNum;
        }
      }
    }
    else
    {
      delete rep;
      rep = NULL;
    }

    if(rep!=NULL)
    {
      rep->plot_main(6,0);
      rep->plot_stats(6,0);
      rep->plot_energy(6,0);
      rep->plot_pixels();
      rep->plot_pixelsTime();

      rep->plotRTime();
      rep->plotFTime();
      rep->plott0();

      rep->plotTemp();
      rep->plotTempTime();
      rep->plotCurr();
      rep->plotCurrTime();
    }
  }
  else
  {
    cout << "\nIncorrect Calibration File\n\n";
    delete rep;
    rep = NULL;
  }

  timeCheck=time(0)-timeCheck;

  cout << "Run Time: " << timeCheck << '\n';

  return rep;
}
