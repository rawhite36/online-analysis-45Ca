//GUI


#ifndef replay_gui_C
#define replay_gui_C

#include "replay_gui.hpp"


tgui::tgui()
{
  //create main frame and connect the exit button
	mainf = new TGMainFrame(gClient->GetRoot(),100,100);
	mainf->Connect("CloseWindow()","tgui",this,"exit()");

  //create tab source
	tabs = new TGTab(mainf,100,100);

  //create drop down menus
  TGPopupMenu* fMenuFile = new TGPopupMenu(gClient->GetRoot());
  TGPopupMenu* fMenuOpti = new TGPopupMenu(gClient->GetRoot());
  TGPopupMenu* fMenuHelp = new TGPopupMenu(gClient->GetRoot());

  TGLayoutHints* fMBItemLayout = new TGLayoutHints(kLHintsTop|kLHintsLeft,0,4,0,0);
  TGLayoutHints* fMBHelpLayout = new TGLayoutHints(kLHintsTop|kLHintsRight);

  //create menu bar
  fMenuBar = new TGMenuBar(mainf,100,20,kHorizontalFrame);

  fMenuBar->AddPopup("&File", fMenuFile, fMBItemLayout);
  fMenuBar->AddPopup("O&ptions", fMenuOpti, fMBItemLayout);
  fMenuBar->AddPopup("&Help", fMenuHelp, fMBHelpLayout);

  //set a few pointers to NULL
	vfr = NULL; hfr = NULL; hfr2 = NULL;
  canMain[0] = NULL; canMain[1] = NULL; canP1 = NULL; canP2 = NULL;
  canP3 = NULL; canWF[0] = NULL; canWF[1] = NULL; canTemp = NULL;

  wfFlag=0;
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


tgui::~tgui()
{
  //statbox.clear();

  mainf->SetCleanup(kDeepCleanup);
	mainf->Cleanup();
  delete mainf;

  if(rep->error!=2 && rep->error!=3)
  {
    delete rep;
    rep = NULL;
  }
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::exit()
{
  delete this;

  gApplication->Terminate(0);
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::input_data(Long64_t n, replay* rpnt)
{
  rep = rpnt;
  nentries = n;
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::maketabs()
{
	//gStyle->SetOptStat(0);

  //create main tab
	hfr = new TGHorizontalFrame(tabs,160,600);

  vfr = new TGVerticalFrame(hfr,800,600);
	canMain[0] = new TRootEmbeddedCanvas("MainCan0",vfr,800,500);
	vfr->AddFrame(canMain[0],new TGLayoutHints(kLHintsExpandX|kLHintsExpandY,5,5,5,5));

  canMain[0]->GetCanvas()->Divide(1,2,.01,.01);
  hfr->AddFrame(vfr,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY|kLHintsRight));

	vfr = new TGVerticalFrame(hfr,200,600);

  hfr2 = new TGHorizontalFrame(vfr,200,45);

  fN2 = new TGNumberEntry(hfr2, 0, 9, -1,TGNumberFormat::kNESInteger,
        TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,5);
  fN2->Connect("ValueSet(Long_t)", "tgui", this, "choosePix()");
  (fN2->GetNumberEntry())->Connect("ReturnPressed()","tgui",this,"choosePix()");
	hfr2->AddFrame(fN2,new TGLayoutHints(kLHintsRight|kLHintsTop,5,0,0,0));

	canTemp = new TRootEmbeddedCanvas("bdLabel",hfr2,60,22);
	hfr2->AddFrame(canTemp,new TGLayoutHints(kLHintsRight|kLHintsTop,5,5,0,0));

	TPaveText* tex = new TPaveText(0.05,0.95,0.95,0.05,"NDC");
	tex->SetBorderSize(0);
	tex->SetFillColor(0);
	tex->SetTextSize(0.55);
	tex->SetTextFont(42);
	tex->SetTextAlign(22);

  tex->AddText("Board");
  tex->Draw();

  vfr->AddFrame(hfr2,new TGLayoutHints(kLHintsRight|kLHintsTop,5,5,5,5));

  hfr2 = new TGHorizontalFrame(vfr,200,45);

  fN3 = new TGNumberEntry(hfr2, 0, 9, -1,TGNumberFormat::kNESInteger,
        TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,7);
  fN3->Connect("ValueSet(Long_t)", "tgui", this, "choosePix()");
  (fN3->GetNumberEntry())->Connect("ReturnPressed()","tgui",this,"choosePix()");
	hfr2->AddFrame(fN3,new TGLayoutHints(kLHintsRight|kLHintsTop,5,0,0,0));

	canTemp = new TRootEmbeddedCanvas("chLabel",hfr2,60,22);
	hfr2->AddFrame(canTemp,new TGLayoutHints(kLHintsRight|kLHintsTop,5,5,0,0));

	TPaveText* tex2 = new TPaveText(0.05,0.95,0.95,0.05,"NDC");
	tex2->SetBorderSize(0);
	tex2->SetFillColor(0);
	tex2->SetTextSize(0.55);
	tex2->SetTextFont(42);
	tex2->SetTextAlign(22);

  tex2->AddText("Channel");
  tex2->Draw();

  vfr->AddFrame(hfr2,new TGLayoutHints(kLHintsRight|kLHintsTop,5,5,5,5));

  hfr2 = new TGHorizontalFrame(vfr,200,45);

  TGTextButton *chsPix = new TGTextButton(hfr2,"Choose Pix.");
  chsPix->Connect("Clicked()","tgui",this,"choosePix()");
	hfr2->AddFrame(chsPix,new TGLayoutHints(kLHintsRight|kLHintsTop,5,0,0,0));

  TGTextButton *chsPixA = new TGTextButton(hfr2,"Choose All Pix.");
  chsPixA->Connect("Clicked()","tgui",this,"chooseAllPix()");
	hfr2->AddFrame(chsPixA,new TGLayoutHints(kLHintsRight|kLHintsTop,5,5,0,0));

  vfr->AddFrame(hfr2,new TGLayoutHints(kLHintsRight|kLHintsTop,5,5,5,5));

	canMain[1] = new TRootEmbeddedCanvas("MainCan1",vfr,200,450);
	vfr->AddFrame(canMain[1],new TGLayoutHints(kLHintsExpandX|kLHintsExpandY,5,5,5,5));

  hfr->AddFrame(vfr,new TGLayoutHints(kLHintsExpandY));

	tabs->AddTab("Main",hfr);

  //create energy tabs
	vfr = new TGVerticalFrame(tabs,100,100);
	canP1 = new TRootEmbeddedCanvas("Set0Can0",vfr,1000,600);
	vfr->AddFrame(canP1,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

	canP1->GetCanvas()->Divide(2,2,.01,.01);
	tabs->AddTab("Energies p1",vfr);

	vfr = new TGVerticalFrame(tabs,100,100);
	canP2 = new TRootEmbeddedCanvas("Set1Can0",vfr,1000,600);
	vfr->AddFrame(canP2,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

	canP2->GetCanvas()->Divide(2,2,.01,.01);
	tabs->AddTab("Energies p2",vfr);

  //create Pixel tabs

	vfr = new TGVerticalFrame(tabs,100,100);

	canP3 = new TRootEmbeddedCanvas("Set2Can0",vfr,1000,600);
	vfr->AddFrame(canP3,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

  canP3->GetCanvas()->Divide(1,2,.01,.01);
	tabs->AddTab("Pixels p1",vfr);

	vfr = new TGVerticalFrame(tabs,100,100);

	canP4 = new TRootEmbeddedCanvas("Set3Can0",vfr,1000,600);
	vfr->AddFrame(canP4,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

	tabs->AddTab("Pixels p2",vfr);

  //create waveform tab
	hfr = new TGHorizontalFrame(tabs,160,600);

	vfr = new TGVerticalFrame(hfr,100,100);

	canWF[0] = new TRootEmbeddedCanvas("WFCan0",vfr,920,600);
	vfr->AddFrame(canWF[0],new TGLayoutHints(kLHintsExpandX|kLHintsExpandY,5,5,5,5));

  hfr->AddFrame(vfr,new TGLayoutHints(kLHintsRight|kLHintsExpandX|kLHintsExpandY));

	vfr = new TGVerticalFrame(hfr,80,600);

	canWF[1] = new TRootEmbeddedCanvas("WFCan1",vfr,80,90);
	vfr->AddFrame(canWF[1],new TGLayoutHints(kLHintsExpandX,5,5,5,5));

  fN1 = new TGNumberEntry(vfr, 0, 9, -1,TGNumberFormat::kNESInteger,
        TGNumberFormat::kNEANonNegative,TGNumberFormat::kNELLimitMinMax,0,nentries-1);
  fN1->Connect("ValueSet(Long_t)", "tgui", this, "selectWF()");
  (fN1->GetNumberEntry())->Connect("ReturnPressed()","tgui",this,"selectWF()");
	vfr->AddFrame(fN1,new TGLayoutHints(kLHintsRight|kLHintsTop,5,5,5,5));

  TGTextButton *show = new TGTextButton(vfr,"Show WF");
  show->Connect("Clicked()","tgui",this,"selectWF()");
	vfr->AddFrame(show,new TGLayoutHints(kLHintsRight|kLHintsTop,5,5,5,5));

  TGTextButton *sbase = new TGTextButton(vfr,"Subtract Baseline");
  sbase->Connect("Clicked()","tgui",this,"subtract_base()");
	vfr->AddFrame(sbase,new TGLayoutHints(kLHintsRight|kLHintsTop,5,5,0,15));

  TGTextButton *appTrapl = new TGTextButton(vfr,"Apply Long Trap. Filt.");
  appTrapl->Connect("Clicked()","tgui",this,"applyLTrap()");
	vfr->AddFrame(appTrapl,new TGLayoutHints(kLHintsRight|kLHintsTop,5,5,15,5));

  TGTextButton *appTraps = new TGTextButton(vfr,"Apply Short Trap. Filt.");
  appTraps->Connect("Clicked()","tgui",this,"applySTrap()");
	vfr->AddFrame(appTraps,new TGLayoutHints(kLHintsRight|kLHintsTop,5,5,5,5));

  hfr->AddFrame(vfr,new TGLayoutHints());

	tabs->AddTab("Waveforms",hfr);

  //create Rise Time tab

	vfr = new TGVerticalFrame(tabs,100,100);
	canP1b = new TRootEmbeddedCanvas("Set0bCan0",vfr,1000,600);
	vfr->AddFrame(canP1b,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

	canP1b->GetCanvas()->Divide(1,2,.01,.01);
	tabs->AddTab("Rise Time",vfr);

  //create Fall Time tab

	vfr = new TGVerticalFrame(tabs,100,100);
	canP2b = new TRootEmbeddedCanvas("Set1bCan0",vfr,1000,600);
	vfr->AddFrame(canP2b,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

	tabs->AddTab("Fall Time",vfr);

  //create T0 tab

	vfr = new TGVerticalFrame(tabs,100,100);
	canP3b = new TRootEmbeddedCanvas("Set2bCan0",vfr,1000,600);
	vfr->AddFrame(canP3b,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

	tabs->AddTab("t0 Dist.",vfr);

  //create Temperature tabs

	vfr = new TGVerticalFrame(tabs,100,100);
	canP1c = new TRootEmbeddedCanvas("Set0cCan0",vfr,1000,600);
	vfr->AddFrame(canP1c,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

	canP1c->GetCanvas()->Divide(2,2,.01,.01);
	tabs->AddTab("Temp. p1",vfr);


	vfr = new TGVerticalFrame(tabs,100,100);
	canP2c = new TRootEmbeddedCanvas("Set1cCan0",vfr,1000,600);
	vfr->AddFrame(canP2c,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

	canP2c->GetCanvas()->Divide(2,2,.01,.01);
	tabs->AddTab("Temp. p2",vfr);

  //create Current tabs

	vfr = new TGVerticalFrame(tabs,100,100);
	canP3c = new TRootEmbeddedCanvas("Set2cCan0",vfr,1000,600);
	vfr->AddFrame(canP3c,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

	canP3c->GetCanvas()->Divide(1,2,.01,.01);
	tabs->AddTab("Curr. p1",vfr);

	vfr = new TGVerticalFrame(tabs,100,100);
	canP4c = new TRootEmbeddedCanvas("Set3cCan0",vfr,1000,600);
	vfr->AddFrame(canP4c,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

	canP4c->GetCanvas()->Divide(1,2,.01,.01);
	tabs->AddTab("Curr. p2",vfr);

  //display
	mainf->AddFrame(fMenuBar,new TGLayoutHints(kLHintsNormal));
	mainf->AddFrame(tabs,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));
	mainf->SetWindowName("Online Analysis");
	mainf->MapSubwindows();
	mainf->Resize(mainf->GetDefaultSize());
	mainf->MapWindow();
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::maintab(Byte_t n)
{
  if(n==0)
    canMain[0]->GetCanvas()->cd(1);
  else if(n==1)
    canMain[0]->GetCanvas()->cd(2);
  else
    canMain[1]->GetCanvas()->cd();
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::choosePix()
{
  canMain[0]->GetCanvas()->GetPad(1)->Clear();
  canMain[0]->GetCanvas()->GetPad(2)->Clear();
  canMain[1]->GetCanvas()->Clear();
  canP1->GetCanvas()->GetPad(1)->Clear();
  canP1->GetCanvas()->GetPad(2)->Clear();
  canP1->GetCanvas()->GetPad(3)->Clear();
  canP1->GetCanvas()->GetPad(4)->Clear();
  canP2->GetCanvas()->GetPad(1)->Clear();
  canP2->GetCanvas()->GetPad(2)->Clear();
  canP2->GetCanvas()->GetPad(3)->Clear();
  canP2->GetCanvas()->GetPad(4)->Clear();

  rep->plot_main(fN2->GetNumberEntry()->GetIntNumber(),fN3->GetNumberEntry()->GetIntNumber());
  rep->plot_stats(fN2->GetNumberEntry()->GetIntNumber(),fN3->GetNumberEntry()->GetIntNumber());
  rep->plot_energy(fN2->GetNumberEntry()->GetIntNumber(),fN3->GetNumberEntry()->GetIntNumber());

  canMain[0]->GetCanvas()->Modified();
  canMain[0]->GetCanvas()->Update();
  canMain[1]->GetCanvas()->Modified();
  canMain[1]->GetCanvas()->Update();
  canP1->GetCanvas()->Modified();
  canP1->GetCanvas()->Update();
  canP2->GetCanvas()->Modified();
  canP2->GetCanvas()->Update();
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::chooseAllPix()
{
  canMain[0]->GetCanvas()->GetPad(1)->Clear();
  canMain[0]->GetCanvas()->GetPad(2)->Clear();
  canMain[1]->GetCanvas()->Clear();
  canP1->GetCanvas()->GetPad(1)->Clear();
  canP1->GetCanvas()->GetPad(2)->Clear();
  canP1->GetCanvas()->GetPad(3)->Clear();
  canP1->GetCanvas()->GetPad(4)->Clear();
  canP2->GetCanvas()->GetPad(1)->Clear();
  canP2->GetCanvas()->GetPad(2)->Clear();
  canP2->GetCanvas()->GetPad(3)->Clear();
  canP2->GetCanvas()->GetPad(4)->Clear();

  rep->plot_main(6,0);
  rep->plot_stats(6,0);
  rep->plot_energy(6,0);

  canMain[0]->GetCanvas()->Modified();
  canMain[0]->GetCanvas()->Update();
  canMain[1]->GetCanvas()->Modified();
  canMain[1]->GetCanvas()->Update();
  canP1->GetCanvas()->Modified();
  canP1->GetCanvas()->Update();
  canP2->GetCanvas()->Modified();
  canP2->GetCanvas()->Update();
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::statsWFc()
{
  canWF[1]->GetCanvas()->cd();
  canWF[1]->GetCanvas()->Clear();
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::statsWFu()
{
  canWF[1]->GetCanvas()->Modified();
  canWF[1]->GetCanvas()->Update();
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::selectWF()
{
  rep->read(fN1->GetNumberEntry()->GetIntNumber(),0);

  canWF[0]->GetCanvas()->cd();
  canWF[0]->GetCanvas()->Clear();

  rep->plotWF(fN1->GetNumberEntry()->GetIntNumber(),0);

  canWF[0]->GetCanvas()->Modified();
  canWF[0]->GetCanvas()->Update();

  wfFlag=1;
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::subtract_base()
{
  if(rep->entry>-1 && wfFlag==1)
  {
    rep->sub_base();

    canWF[0]->GetCanvas()->cd();
    canWF[0]->GetCanvas()->Clear();

    rep->plotWF(rep->entry,0);

    canWF[0]->GetCanvas()->Modified();
    canWF[0]->GetCanvas()->Update();
  }
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::applyLTrap()
{
  if(rep->entry>-1 && wfFlag!=0)
  {
    rep->sub_base();
    rep->long_trap();

    canWF[0]->GetCanvas()->cd();
    canWF[0]->GetCanvas()->Clear();

    rep->plotWF(rep->entry,1);

    canWF[0]->GetCanvas()->Modified();
    canWF[0]->GetCanvas()->Update();

    wfFlag=2;
  }
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::applySTrap()
{
  if(rep->entry>-1 && wfFlag!=0)
  {
    rep->sub_base();
    rep->short_trap();

    canWF[0]->GetCanvas()->cd();
    canWF[0]->GetCanvas()->Clear();

    rep->plotWF(rep->entry,2);

    canWF[0]->GetCanvas()->Modified();
    canWF[0]->GetCanvas()->Update();

    wfFlag=3;
  }
}

#endif
