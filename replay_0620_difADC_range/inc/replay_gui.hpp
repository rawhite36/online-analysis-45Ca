//class header


#ifndef replay_gui_H
#define replay_gui_H

#include <TQObject.h>
#include <TStyle.h>
#include <TPaveText.h>
#include <TROOT.h>
#include <TClass.h>
#include <TApplication.h>
#include <TVirtualX.h>
#include <TVirtualPadEditor.h>
#include <TGResourcePool.h>
#include <TGListBox.h>
#include <TGListTree.h>
#include <TGFSContainer.h>
#include <TGClient.h>
#include <TGFrame.h>
#include <TGIcon.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGMsgBox.h>
#include <TGMenu.h>
#include <TGCanvas.h>
#include <TGComboBox.h>
#include <TGTab.h>
#include <TGSlider.h>
#include <TGDoubleSlider.h>
#include <TGFileDialog.h>
#include <TGTextEdit.h>
#include <TGShutter.h>
#include <TGProgressBar.h>
#include <TGColorSelect.h>
#include <RQ_OBJECT.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TH1.h>
#include <TH2.h>
#include <TRandom.h>
#include <TSystem.h>
#include <TSystemDirectory.h>
#include <TEnv.h>
#include <TFile.h>
#include <TKey.h>
#include <TGDockableFrame.h>
#include <TGFontDialog.h>


class tgui
{
	RQ_OBJECT("tgui");

	TGMainFrame* mainf;
  TGMenuBar* fMenuBar;
	TGTab* tabs;
	TGVerticalFrame* vfr;
  TGHorizontalFrame* hfr;
  TGHorizontalFrame* hfr2;
	TRootEmbeddedCanvas* canMain[2];
	TRootEmbeddedCanvas* canP1;
	TRootEmbeddedCanvas* canP2;
	TRootEmbeddedCanvas* canP3;
	TRootEmbeddedCanvas* canP4;
  TRootEmbeddedCanvas* canWF[2];
  TRootEmbeddedCanvas* canP1b;
  TRootEmbeddedCanvas* canP2b;
  TRootEmbeddedCanvas* canP3b;
  TRootEmbeddedCanvas* canP1c;
  TRootEmbeddedCanvas* canP2c;
  TRootEmbeddedCanvas* canP3c;
  TRootEmbeddedCanvas* canP4c;
  TRootEmbeddedCanvas* canTemp;
  TGNumberEntry* fN1;
  TGNumberEntry* fN2;
  TGNumberEntry* fN3;
  replay* rep;
  Long64_t nentries;
  Byte_t wfFlag;

	public:
		tgui();
		~tgui();
		void exit();
    void input_data(Long64_t, replay*);
    void maketabs();
		void maintab(Byte_t);
    void choosePix();
    void chooseAllPix();
		void set0(Byte_t n) { canP1->GetCanvas()->cd(n); }
		void set1(Byte_t n) { canP2->GetCanvas()->cd(n); }
    void set2(Byte_t n) { canP3->GetCanvas()->cd(n); }
    void set3() { canP4->GetCanvas()->cd(); }
    void selCanWF() { canWF[0]->GetCanvas()->cd(); }
    void statsWFc();
    void statsWFu();
    void selectWF();
    void applyLTrap();
    void applySTrap();
    void subtract_base();
    void set0b(Byte_t n) { canP1b->GetCanvas()->cd(n); }
    void set1b() { canP2b->GetCanvas()->cd(); }
    void set2b() { canP3b->GetCanvas()->cd(); }
    void set0c(Byte_t n) { canP1c->GetCanvas()->cd(n); }
    void set1c(Byte_t n) { canP2c->GetCanvas()->cd(n); }
    void set2c(Byte_t n) { canP3c->GetCanvas()->cd(n); }
    void set3c(Byte_t n) { canP4c->GetCanvas()->cd(n); }
};

#endif
