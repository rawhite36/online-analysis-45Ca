//replay class header


#ifndef replay_H
#define replay_H

#define SRT 10
#define SFT 4
#define LRT 300
#define LFT 100
#define DEC 1000.

#include <TH1.h>
#include <TGraph.h>
#include <TF1.h>
#include <fstream>
#include <vector>
#include <TCanvas.h>
#include <iostream>
#include <math.h>


class tgui;

class replay
{
  Int_t fNum;
  Int_t fCnt;
  Long64_t* gCnt;
  Byte_t initFlag;
  Byte_t statInitFlag;
  Double_t calib[6][8][3];
  Byte_t clbFlag;
  ifstream* fin;
  ifstream* fin2;
  ifstream* fin3;
  Byte_t result;
  Int_t bd;
  Int_t ch;
  ULong64_t timR;
  ULong64_t timC;
  Int_t length;
  Int_t bd2;
  Int_t ch2;
  Byte_t result2;
  ULong64_t tim2;
  Float_t fallTime;
  Float_t riseTime;
  Float_t v0;
  Float_t t0;
  Float_t chi;
  Short_t* data;
  Int_t bmean;
  Double_t* strap;
  Double_t* ltrap;
  Short_t peakSep;
  Short_t pkf[2];
  Double_t dec[6][8];
  Long64_t decCnt[6][8];
  Double_t s;
  Int_t p, d;
  Int_t strap_length;
  Int_t ltrap_length;
  Short_t sfilt[2*SRT+SFT+1];
  Short_t lfilt[2*LRT+LFT+1];
  vector<TCanvas*> can;
  vector<TGraph*> gr;
  vector<ULong64_t> twindow[2];
  vector<Byte_t> mpixF;
  vector<Byte_t> pileF;
  Float_t* rtime;
  Float_t* itime;
  Float_t* ftime;
  Double_t** slowData;
	tgui* gui;
  TH1D* hist;
  TH1D** histE;
  TGraph* grWF;
  TGraph** gr2;

  Byte_t pile_ident();
  void find_width(Short_t []);
  void find_peaks(Byte_t []);
  Byte_t mpix_check();
  Double_t lfilt_extract();

  public:
    Long64_t* nentriesT;
    Long64_t nentries;
    Long64_t nentries2;
    Long64_t nentries3;
    Long64_t entry;
    Long64_t entry2;
    Long64_t entry3;
    Double_t** engy_spec;
    Byte_t** classType;
    Int_t** bdchf;
    Int_t*** bdch;
    ULong64_t** timWF;
    Byte_t error;

    replay(Int_t);
    ~replay();

    Byte_t openf(string);
    Byte_t openf2(string);
    Byte_t openf3(string);
    void closef();
    void read();
    void read(Long64_t,Byte_t);
    void read2();
    void read3();
    Byte_t calibrate();
    void getDecConst();
    void exp_fit();
    void sub_base();
    void short_trap();
    void long_trap();
    void view(Byte_t);
    void clear_graphs();
    Byte_t classify();
    void engy_construct(Byte_t);
    void sepPile(Short_t*,Short_t*);
    void plot_main(Byte_t,Byte_t);
    void plot_stats(Byte_t,Byte_t);
    void plot_energy(Byte_t,Byte_t);
    void plot_pixels();
    void plot_pixelsTime();
    void plotWF(Long64_t,Byte_t);
    void plotRTime();
    void plotFTime();
    void plott0();
    void plotTemp();
    void plotTempTime();
    void plotCurr();
    void plotCurrTime();
};


#endif
